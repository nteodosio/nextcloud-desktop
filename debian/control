Source: nextcloud-desktop
Section: net
Priority: optional
Maintainer: ownCloud for Debian maintainers <pkg-owncloud-maintainers@lists.alioth.debian.org>
Uploaders: Sandro Knauß <hefee@debian.org>, Alf Gaida <agaida@siduction.org>
Build-Depends: appstream,
               cmake (>= 3.16~),
               debhelper-compat (= 13),
               doxygen,
               extra-cmake-modules (>= 5.16.0~),
               libcloudproviders-dev,
               libcmocka-dev,
               libdbus-1-dev,
               libinotify-dev [kfreebsd-any],
               libkf5coreaddons-dev (>= 5.16.0~),
               libkf5kio-dev (>= 5.16.0~),
               libqt5svg5-dev,
               libqt5webkit5-dev,
               libqt5websockets5-dev,
               librsvg2-bin,
               libsqlite3-dev (>= 3.8.0~),
               libssl-dev (>= 1.1~),
               pkg-config,
               python3-sphinx,
               qt5keychain-dev,
               qtbase5-dev (>= 5.15.0~),
               qtdeclarative5-dev,
               qtkeychain-qt5-dev,
               qtquickcontrols2-5-dev,
               qttools5-dev,
               qttools5-dev-tools,
               qtwebengine5-dev (>= 5.15~),
               texlive-latex-base,
               xauth,
               xvfb,
               zlib1g-dev
Build-Depends-Indep: dh-sequence-sphinxdoc, jdupes
Rules-Requires-Root: no
Standards-Version: 4.6.1
Homepage: https://nextcloud.com/install/#install-clients
Vcs-Browser: https://salsa.debian.org/owncloud-team/nextcloud-desktop
Vcs-Git: https://salsa.debian.org/owncloud-team/nextcloud-desktop.git

Package: nextcloud-desktop
Architecture: any
Depends: libnextcloudsync0 (= ${binary:Version}),
         libqt5sql5-sqlite,
         nextcloud-desktop-common,
         nextcloud-desktop-l10n,
         qml-module-qt-labs-platform,
         qml-module-qtgraphicaleffects,
         qml-module-qtqml-models2,
         qml-module-qtquick-controls2,
         qml-module-qtquick-layouts,
         qml-module-qtquick-window2,
         qml-module-qtquick2,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: nextcloud-desktop-doc
Description: Nextcloud folder synchronization tool
 The Nextcloud desktop app lets you always have your latest files wherever
 you are. Just specify one or more folders on the local machine to and a server
 to synchronize to. You can configure more computers to synchronize to the same
 server and any change to the files on one computer will silently and reliably
 flow across to every other.
 .
 nextcloud-desktop provides the graphical client specialising in
 synchronizing with cloud storage provided by Nextcloud.

Package: nextcloud-desktop-doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Built-Using: ${sphinxdoc:Built-Using}
Section: doc
Description: Nextcloud folder synchronization - documentation
 The Nextcloud desktop app lets you always have your latest files wherever
 you are. Just specify one or more folders on the local machine to and a server
 to synchronize to. You can configure more computers to synchronize to the same
 server and any change to the files on one computer will silently and reliably
 flow across to every other.
 .
 This package provides the documentation.

Package: libnextcloudsync0
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Section: libs
Description: Nextcloud folder synchronization - libraries
 The Nextcloud desktop app lets you always have your latest files wherever
 you are. Just specify one or more folders on the local machine to and a server
 to synchronize to. You can configure more computers to synchronize to the same
 server and any change to the files on one computer will silently and reliably
 flow across to every other.
 .
 This package provides the library for nextcloud-desktop.

Package: libnextcloudsync-dev
Architecture: any
Multi-Arch: same
Depends: libnextcloudsync0 (= ${binary:Version}), ${misc:Depends}
Section: libdevel
Description: Nextcloud folder synchronization - development files
 The Nextcloud desktop app lets you always have your latest files wherever
 you are. Just specify one or more folders on the local machine to and a server
 to synchronize to. You can configure more computers to synchronize to the same
 server and any change to the files on one computer will silently and reliably
 flow across to every other.
 .
 This package contains the library development files.

Package: nextcloud-desktop-common
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Nextcloud folder synchronization - common data
 The Nextcloud desktop app lets you always have your latest files wherever
 you are. Just specify one or more folders on the local machine to and a server
 to synchronize to. You can configure more computers to synchronize to the same
 server and any change to the files on one computer will silently and reliably
 flow across to every other.
 .
 This package provides data that is shared between different packages.

Package: nextcloud-desktop-l10n
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Section: localization
Description: Nextcloud folder synchronization - localization
 The Nextcloud desktop app lets you always have your latest files wherever
 you are. Just specify one or more folders on the local machine to and a server
 to synchronize to. You can configure more computers to synchronize to the same
 server and any change to the files on one computer will silently and reliably
 flow across to every other.
 .
 This package provides the localization.

Package: nextcloud-desktop-cmd
Architecture: any
Depends: libnextcloudsync0 (= ${binary:Version}),
         libqt5sql5-sqlite,
         nextcloud-desktop-common,
         nextcloud-desktop-l10n,
         ${misc:Depends},
         ${shlibs:Depends}
Description: folder synchronization with an Nextcloud server - cmd client
 The Nextcloud desktop app lets you always have your latest files wherever
 you are. Just specify one or more folders on the local machine to and a server
 to synchronize to. You can configure more computers to synchronize to the same
 server and any change to the files on one computer will silently and reliably
 flow across to every other.
 .
 This package provides the command line client specialising in
 synchronizing with cloud storage provided by Nextcloud.

Package: dolphin-nextcloud
Architecture: any
Multi-Arch: same
Section: kde
Depends: dolphin (>= 4:15.12.1),
         libnextcloudsync0 (= ${binary:Version}),
         nextcloud-desktop,
         ${misc:Depends},
         ${shlibs:Depends}
Enhances: dolphin
Description: Nextcloud integration for Dolphin
 The Nextcloud desktop app lets you always have your latest files wherever
 you are. Just specify one or more folders on the local machine to and a server
 to synchronize to. You can configure more computers to synchronize to the same
 server and any change to the files on one computer will silently and reliably
 flow across to every other.
 .
 Dolphin Nextcloud is an extension that integrates the Nextcloud web service
 with your Plasma Desktop (KDE).

Package: nautilus-nextcloud
Architecture: all
Section: gnome
Depends: nautilus,
         nextcloud-desktop (<< ${source:Version}.1~),
         nextcloud-desktop (>= ${source:Version}),
         nextcloud-desktop-common,
         python3-nautilus,
         ${misc:Depends}
Suggests: nautilus-script-manager
Enhances: nautilus
Description: Nextcloud integration for Nautilus
 The Nextcloud desktop app lets you always have your latest files wherever
 you are. Just specify one or more folders on the local machine to and a server
 to synchronize to. You can configure more computers to synchronize to the same
 server and any change to the files on one computer will silently and reliably
 flow across to every other.
 .
 Nautilus Nextcloud is an extension that integrates the Nextcloud web service
 with your GNOME Desktop.

Package: nemo-nextcloud
Architecture: all
Depends: nemo,
         nemo-python,
         nextcloud-desktop (<< ${source:Version}.1~),
         nextcloud-desktop (>= ${source:Version}),
         nextcloud-desktop-common,
         ${misc:Depends}
Enhances: nemo
Description: Nextcloud integration for Nemo
 The Nextcloud desktop app lets you always have your latest files wherever
 you are. Just specify one or more folders on the local machine to and a server
 to synchronize to. You can configure more computers to synchronize to the same
 server and any change to the files on one computer will silently and reliably
 flow across to every other.
 .
 Nemo Nextcloud is an extension that integrates the Nextcloud web service with
 your Cinnamon Desktop.

Package: caja-nextcloud
Architecture: all
Depends: caja,
         nextcloud-desktop (<< ${source:Version}.1~),
         nextcloud-desktop (>= ${source:Version}),
         nextcloud-desktop-common,
         python3-caja,
         python3-gi,
         ${misc:Depends}
Enhances: caja
Description: Nextcloud integration for Caja
 The Nextcloud desktop app lets you always have your latest files wherever
 you are. Just specify one or more folders on the local machine to and a server
 to synchronize to. You can configure more computers to synchronize to the same
 server and any change to the files on one computer will silently and reliably
 flow across to every other.
 .
 Caja Nextcloud is an extension that integrates the Nextcloud web service with
 your MATE Desktop.
